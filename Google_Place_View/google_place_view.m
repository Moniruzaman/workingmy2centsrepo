//
//  google_place_view.m
//  My2Cents
//
//  Created by Rasel_cse07 on 8/6/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "google_place_view.h"

@interface google_place_view ()

@end

@implementation google_place_view

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
