//
//  ViewController.m
//  My2Cents
//
//  Created by Rasel_cse07 on 7/23/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import "ViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "SingleTon.h"
#import "QuestionPageViewController.h"
#import "AllBusinessView.h"
#import "google_place_view.h"

@interface ViewController ()

@end

@implementation ViewController{

        
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.navigationController.navigationBarHidden=YES;
    [copyrightview setBackgroundColor:[[AppDelegate ShareDelegate] colorFromHexString:@"14a2f0"]];
    txtbusiness.delegate=self;
    txtbusiness1.delegate=self;
    
    if (nil == locationManager)
        locationManager = [[CLLocationManager alloc] init];
    
    locationManager.delegate = self;
    //Configure Accuracy depending on your needs, default is kCLLocationAccuracyBest
    locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
    
    // Set a movement threshold for new events.
    locationManager.distanceFilter = 500; // meters
    
    [locationManager startUpdatingLocation];

}
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation{
    
    // Added by rahid start
    // checking location is updated or not. Only the view loading time the map moved & zoomed to current location
    
   
    [[SingleTon instance] setCurrentLocationlat:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]];
    [[SingleTon instance] setCurrentLocationlon:[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]];
    
    NSLog(@"lat%@",[[SingleTon instance] CurrentLocationlat]);
    NSLog(@"lon%@",[[SingleTon instance] CurrentLocationlon]);
    
}
-(void)viewWillAppear:(BOOL)animated
{
  self.navigationController.navigationBarHidden=YES;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{

   /*
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
    [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
    [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Done", @"Done") style:UIBarButtonItemStyleDone target: self action:@selector(doneNumbeButton)],
    nil];
    [numberToolbar sizeToFit];
    txtMobileNumber.inputAccessoryView = numberToolbar;
    
    
    -(void)doneNumbeButton{
    
    [txtMobileNumber resignFirstResponder];
    }

    */
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    [numberToolbar sizeToFit];
    textField.inputAccessoryView = numberToolbar;

    return YES;
}
-(void)cancelNumberPad{
    [txtbusiness resignFirstResponder];
    [txtbusiness1 resignFirstResponder];
}
-(void)doneWithNumberPad{
    //NSString *numberFromTheKeyboard = [sender text];
    [txtbusiness resignFirstResponder];
    [txtbusiness1 resignFirstResponder];
    
}
- (IBAction)BarScanAction:(id)sender {

    
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    // present and release the controller
    [self presentViewController:reader animated:YES completion:nil];
}


- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    txtbusiness.text = symbol.data;
    txtbusiness1.text=symbol.data;
    
    // EXAMPLE: do something useful with the barcode image
   // resultImage.image = [info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    [reader dismissViewControllerAnimated:YES completion:nil];
}



- (IBAction)submit_action:(id)sender {
    
    if ([[UIScreen mainScreen] bounds].size.height!=568) {
        
        if ([txtbusiness1.text length]>0)
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //NSString * url=@"http://my2cents.biz/api.my2cents/getQuestions.php?vendorCode=057805";
            NSString * url=[NSString stringWithFormat:@"http://my2cents.biz/api.my2cents/getQuestions.php?vendorCode=%@",txtbusiness.text];
            [self RequestData:url];
        }
        else
        {
            [[AppDelegate ShareDelegate] customAlert:@"Information!" withMessage:@"Please Enter Valid Business Code"];
        }
    }
    else
    {
        if ([txtbusiness.text length]>0)
        {
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            //NSString * url=@"http://my2cents.biz/api.my2cents/getQuestions.php?vendorCode=057805";
            NSString * url=[NSString stringWithFormat:@"http://my2cents.biz/api.my2cents/getQuestions.php?vendorCode=%@",txtbusiness.text];
            [self RequestData:url];
        }
        else
        {
            [[AppDelegate ShareDelegate] customAlert:@"Information!" withMessage:@"Please Enter Valid Business Code"];
        }
    
    }
    
    
}
#pragma - mark Post User Data To Server
- (void)RequestData:(NSString *)url_request
{
    
    NSString *requestingUrl = [NSString stringWithFormat:@"%@",url_request] ;
    NSLog(@"URL-%@",requestingUrl);
    
    NSURL* url = [NSURL URLWithString:requestingUrl];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    request.shouldAttemptPersistentConnection = NO;
    
    
    [request setCompletionBlock:^
     {
         NSString *tempString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];//[request responseData];
         
         if ([request responseStatusCode] != 200) {
             
         } else {
             
             NSLog(@"%@",tempString);
             NSDictionary *dic=[tempString JSONValue];
             NSLog(@"jsonDictionary %@",dic);
             
             NSArray *dataarray=[[dic valueForKey:@"business"] valueForKey:@"questions"];
             if (dataarray ==nil)
             {
                 [[AppDelegate ShareDelegate] customAlert:[dic valueForKey:@"msg_type"] withMessage:[dic valueForKey:@"msg"]];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             }
             else
             {
                 [[SingleTon instance] setQuestionarray:dataarray];
                 [[SingleTon instance] setBusinessName:[[dic valueForKey:@"business"] valueForKey:@"name"]];
                 [[SingleTon instance] setVendoreCode:[[dic valueForKey:@"business"] valueForKey:@"vencode"]];
                 
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 QuestionPageViewController *ques=[[QuestionPageViewController alloc] initWithNibName:@"QuestionPageViewController" bundle:nil];
                 //[self.navigationController.navigationBar setTitleTextAttributes:[[AppDelegate ShareDelegate] ReturnCUSTOMNacTitleAttribute]];
                 [self.navigationController pushViewController:ques animated:YES];
                 
             }
             
            
         }
         
     }];
    
	[request setFailedBlock:^
     {
         
     }];
    
	[request startAsynchronous];
    
}
//-----
- (IBAction)My_piggy_action:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString * url=@"http://my2cents.biz/api.my2cents/getAllAnswers.php?custUIIcode=Not+Available";
    [self RequestDataForPiggy:url];
    
}
- (void)RequestDataForPiggy:(NSString *)url_request
{
    
    NSString *requestingUrl = [NSString stringWithFormat:@"%@",url_request] ;
    NSLog(@"URL-%@",requestingUrl);
    
    NSURL* url = [NSURL URLWithString:requestingUrl];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    request.shouldAttemptPersistentConnection = NO;
    
    
    [request setCompletionBlock:^
     {
         NSString *tempString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];//[request responseData];
         
         if ([request responseStatusCode] != 200) {
             
         } else {
             
             NSLog(@"%@",tempString);
             NSDictionary *dic=[tempString JSONValue];
             NSLog(@"jsonDictionary %@",dic);
             
             NSArray *dataarray=[dic valueForKey:@"all_ans"];
             if (dataarray ==nil)
             {
                 //[[AppDelegate ShareDelegate] customAlert:[dic valueForKey:@"msg_type"] withMessage:[dic valueForKey:@"msg"]];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             }
             else
             {
                 [[SingleTon instance] setAllPiggyBusinessData:dataarray];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                 AllBusinessView *ques=[[AllBusinessView alloc] initWithNibName:@"AllBusinessView" bundle:nil];
                 [self.navigationController pushViewController:ques animated:YES];
                 
             }
             
             
         }
         
     }];
    
	[request setFailedBlock:^
     {
         
     }];
    
	[request startAsynchronous];
    
}
//---
- (IBAction)Find_Business_Action:(id)sender {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    NSString * url=[NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/search/json?key=AIzaSyDA1NLbJueON5XLGQieoQg5hla-GNLUVEQ&location=23.780086,90.3934546&radius=500.0&sensor=false&types=restaurant%%7Cbank%%7Cbook_store%%7Cbowling_alley%%7Ccafe%%7Ccar_dealer%%7Ccar_rental%%7Cclothing_store%%7Cconvenience_store%%7Cdepartment_store%%7Celectronics_store%%7Cflorist%%7Cfood%%7Cgrocery_or_supermarket%%7Cgym%%7Chardware_store%%7Chome_goods_store%%7Chospital%%7Cjewelry_store%%7Claundry%%7Clawyer%%7Cliquor_store%%7Clodging%%7Cmeal_delivery%%7Cmovie_theater%%7Cmoving_company%%7Cmuseum%%7Cpet_store%%7Cpharmacy%%7Crestaurant%%7Cshoe_store%%7Cshopping_mall%%7Cspa%%7Cstore"];
    [self RequestGoogleNearestData:url];
}
- (void)RequestGoogleNearestData:(NSString *)url_request
{
    
    NSString *requestingUrl = [NSString stringWithFormat:@"%@",url_request] ;
    NSLog(@"URL-%@",requestingUrl);
    
    NSURL* url = [NSURL URLWithString:requestingUrl];
    
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
    [request addRequestHeader:@"Content-Type" value:@"application/json"];
    request.shouldAttemptPersistentConnection = NO;
    
    
    [request setCompletionBlock:^
     {
         NSString *tempString = [[NSString alloc] initWithData:[request responseData] encoding:NSUTF8StringEncoding];//[request responseData];
         
         if ([request responseStatusCode] != 200) {
             
         } else {
             
             NSLog(@"%@",tempString);
             NSDictionary *dic=[tempString JSONValue];
             //NSLog(@"jsonDictionary %@",dic);
             
             NSArray *dataarray=[dic valueForKey:@"results"];
             NSLog(@"jsonDictionary %@",dataarray);
             if ([dataarray count] ==0)
             {
                 [[AppDelegate ShareDelegate] customAlert:@"Information" withMessage:[dic valueForKey:@"status"]];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
             }
             else
             {
                 [[SingleTon instance] setGooglebusinessArray:dataarray];
                 [MBProgressHUD hideHUDForView:self.view animated:YES];
                 
                 google_place_view *ques=[[google_place_view alloc] initWithNibName:@"google_place_view" bundle:nil];
                 [self.navigationController pushViewController:ques animated:YES];
                 
             }
             
             
         }
         
     }];
    
	[request setFailedBlock:^
     {
         
     }];
    
	[request startAsynchronous];
    
}

@end
