//
//  ViewController.h
//  My2Cents
//
//  Created by Rasel_cse07 on 7/23/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "ZBarSDK.h"

@interface ViewController : UIViewController<CLLocationManagerDelegate,UITextFieldDelegate,UIAlertViewDelegate, ZBarReaderDelegate >
{

    __weak IBOutlet UITextField *txtbusiness;
    IBOutlet UITextField *txtbusiness1;
    
    IBOutlet UIView *copyrightview;
    
    CLLocationManager *locationManager;
    
}
- (IBAction)BarScanAction:(id)sender;
- (IBAction)submit_action:(id)sender;

- (IBAction)My_piggy_action:(id)sender;
- (IBAction)Find_Business_Action:(id)sender;



@property (strong, nonatomic) NSMutableArray * allowedBarcodeTypes;
@property (strong, nonatomic) NSMutableArray * foundBarcodes;


@end
