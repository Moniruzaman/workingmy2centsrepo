//
//  AppDelegate.h
//  My2Cents
//
//  Created by Rasel_cse07 on 7/23/14.
//  Copyright (c) 2014 Rasel_cse07. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
}

@property (strong, nonatomic) UIWindow *window;
- (UIColor *) colorFromHexString:(NSString *)hexString;
- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message;

-(NSDictionary*)ReturnCUSTOMNacTitleAttribute;
+(AppDelegate *)ShareDelegate;




@end
